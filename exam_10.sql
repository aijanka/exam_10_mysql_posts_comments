CREATE SCHEMA news_block DEFAULT CHARACTER SET utf8mb4;
USE news_block;

CREATE TABLE news (
	id INT NOT NULL AUTO_INCREMENT,
    PRIMARY KEY(id),
    title VARCHAR(255) NOT NULL,
    content TEXT NOT NULL,
    image VARCHAR(255),
    date_added DATETIME DEFAULT CURRENT_TIMESTAMP
);

SELECT * FROM news;

CREATE TABLE comments (
	id INT NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (id),
    author VARCHAR(255),
    content TEXT NOT NULL,
    news_id INT NOT NULL,
    INDEX QWE_news_id (news_id),
    CONSTRAINT QWE_news
		FOREIGN KEY (news_id)
        REFERENCES news_block.news(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

select * from comments WHERE news_id = 2;

insert into comments (author, content, news_id) 
values ('a', 'aaaa', 1);



INSERT INTO news (title, content)
VALUES ('Ким Чен Ын объявил о приостановке ядерных и ракетных испытаний КНДР', 'Лидер КНДР Ким Чен Ын объявил о приостановке испытаний ядерного оружия и межконтинентальных баллистических ракет и закрытии полигона для ядерных испытаний с 21 апреля.

По его словам, после постановки ядерных средств на вооружение Северной Корее больше не нужно проводить испытания, цитирует РИА Новости сообщение северокорейского агентства ЦТАК.');

SELECT * FROM news_block.comments;

INSERT INTO comments(content, news_id)
VALUES 
		('ну и что', 4),
        ('aga', 2),
        ('что', 2),
        ('ну и что', 1);