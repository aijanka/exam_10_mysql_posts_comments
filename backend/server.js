const express = require('express');
const cors = require('cors');
const mysql = require('mysql');

const app = express();
const port = 8000;
const news = require('./app/news');
const comments = require('./app/comments');

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '0777512992Aa',
    database: 'news_block'
});

connection.connect(err => {
    if(err) throw err;

    app.use('/news', news(connection));
    app.use('/comments', comments(connection));

    app.listen(port);
})