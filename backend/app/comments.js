const express = require('express');

const router = express.Router();

const createRouter = db => {
    router.get('/', (req, res) => {
        if(req.query.news_id){
            const id = req.query.news_id;
            db.query(`SELECT * FROM news_block.comments WHERE news_id=${id}`,
                (error, results) => {
                if(error) throw error;
                console.log(results);
                res.send(results);
                })
        } else {
            db.query('SELECT * FROM news_block.comments', (error, results) => {
                if(error) throw error;
                res.send(results);
            })
        }
    });

    router.post('/', (req, res) => {
        const comment = req.body;
        console.log(req.body);
        if(!comment.author) comment.author = 'Anonymous';
        db.query(`INSERT INTO news_block.comments (author, content, news_id)` +
                'VALUES (?, ?, ?)',
                [comment.author, comment.content, comment.news_id],
                (error, results) => {
                if(error) throw error;
                comment.id = results.insertId;
                res.send(comment);
                }
        )
    });

     router.delete('/', (req, res) => {
         const id = req.body.id;
         db.query(`DELETE FROM news_block.comments WHERE id=${id}`, (error, results) => {
             if(error) throw error;
         });
     })

    return router;
};

module.exports = createRouter;