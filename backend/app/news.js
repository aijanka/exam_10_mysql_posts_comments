const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = db => {
    router.get('/', (req, res) => {
        db.query('SELECT * FROM news_block.news', function (error, results) {
            if(error) throw error;
            res.send(results);
        })
    });
    router.post('/', upload.single('image'), (req,res) => {
        const post = req.body;

        if(req.file) {
            post.image = req.file.filename;
        } else {
            post.image = null
        }

        db.query(
            'INSERT INTO news_block.news (title, content, image)' +
            'VALUES (?, ?, ?)', [post.title, post.content, post.image],
            (error, results) => {
                if(error) throw error;
                post.id = results.insertId;
                res.send(post);
            }
        );

    });

    router.get('/:id', (req, res) => {
        const id = req.params.id;
        db.query(`SELECT * FROM news_block.news WHERE id=${id}`, (error, results) => {
            if(error) throw error;
            res.send(results);
        })
    });

    router.delete('/:id', (req,res) => {
        const id = req.params.id;
        db.query(`DELETE FROM news_block.news WHERE id=${id}`, (error, results) => {
            if(error) throw error;
        })
    });

    return router;
};

module.exports = createRouter;