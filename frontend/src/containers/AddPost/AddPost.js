import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, ControlLabel, FormControl, FormGroup, Grid} from "react-bootstrap";
import {addPost} from "../../store/actions";


class AddPost extends Component {
    state = {
        title: '',
        content: '',
        image: ''
    }


    saveCurrentTitle = event => {
        this.setState({title: event.target.value});
    };

    saveCurrentContent = event => {
        this.setState({content: event.target.value});
    }

    saveCurrentImage = event => {
        this.setState({[event.target.name]: event.target.files[0]});
    };

    addPost = event => {
        event.preventDefault();
        console.log(this.state);
        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });
        this.props.addPost(formData);
        this.props.history.push('/news');
        // this.setState({message: '', author: '', img: ''})
        // this.forceUpdate(); //get rid of it
    };

    render() {
        return (
            <Grid>
                <form>

                    <FormGroup controlId="formInlineName">
                        <ControlLabel>Title</ControlLabel>{' '}
                        <FormControl type="text" placeholder="Title" name='title' onChange={this.saveCurrentTitle}/>
                    </FormGroup>

                    <FormGroup controlId="formControlsTextarea">
                        <ControlLabel>Content</ControlLabel>
                        <FormControl componentClass="textarea" placeholder="Content" name='content'
                                     onChange={this.saveCurrentContent}/>
                    </FormGroup>

                    <FormGroup controlId="formInlineEmail">
                        <ControlLabel>Picture</ControlLabel>{' '}
                        <FormControl
                            type="file"
                            placeholder="Your picture"
                            name='image'
                            onChange={this.saveCurrentImage}
                        />
                    </FormGroup>


                    <Button type="submit" onClick={this.addPost}>Submit</Button>
                </form>
            </Grid>
        )
    }
}

// const mapStateToProps = state => ({
//     posts: state.posts
// });

const mapDispatchToProps = dispatch => ({
    addPost: (post) => dispatch(addPost(post))
});

export default connect(null, mapDispatchToProps)(AddPost);