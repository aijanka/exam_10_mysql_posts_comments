import React, {Component} from 'react';
import {Grid, Jumbotron, Panel} from "react-bootstrap";
import {connect} from "react-redux";
import {addComment, getComments, getPost} from "../../store/actions";
import SendMessageForm from "../../components/AddCommentForm/AddCommentForm";

class PostFull extends Component {
    state = {
        currentMessage: '',
        currentAuthor: '',
        posts: []
    };

    id = this.props.match.params.id;
    componentDidMount() {
        this.props.getPost(this.id);
        this.props.getComments(this.id);
    }

    saveCurrentMessage(event) {
        this.setState({currentMessage: event.target.value});
    }

    saveCurrentAuthor(event) {
        this.setState({currentAuthor: event.target.value});
    }

    addPost = (event) => {
        event.preventDefault();
        const comment = {
            content: this.state.currentMessage,
            author: this.state.currentAuthor,
            news_id: this.id
        };
        this.props.addComment(comment);
        this.setState({currentMessage: '', currentAuthor: ''})
        // this.forceUpdate();
    }
    render() {
        const emptyPost = {
            title: '',
            content: ''
        };

        const post = this.props.post ? this.props.post : emptyPost;

        return (
            <Grid>
                <Jumbotron>
                    <h2>{post.title}</h2>
                    <p>{post.content}</p>
                </Jumbotron>
                {Object.keys(this.props.comments).map(key => (
                    <Panel>
                        <Panel.Heading>{this.props.comments[key].author}</Panel.Heading>
                        <Panel.Body>{this.props.comments[key].content}</Panel.Body>
                    </Panel>
                ))}
                <SendMessageForm
                    currentMessage={(event) => this.saveCurrentMessage(event)}
                    currentAuthor={(event) => this.saveCurrentAuthor(event)}
                    author={this.state.currentAuthor}
                    message={this.state.currentMessage}
                    addPost={this.addPost}
                />
            </Grid>
        )
    }
}

const mapStateToProps = state => ({
    post: state.currentPost,
    comments: state.currentComments
})

const mapDispatchToProps = dispatch => ({
    getPost: (id) => dispatch(getPost(id)),
    getComments: (id) => dispatch(getComments(id)),
    addComment: (comment) => dispatch(addComment(comment))
});

export default connect(mapStateToProps, mapDispatchToProps)(PostFull);