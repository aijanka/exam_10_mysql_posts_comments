import React, {Component} from 'react';
import {connect} from "react-redux";
import {Grid} from "react-bootstrap";
import {deletePost, getPosts} from "../../store/actions";
import Post from "../../components/Post/Post";

class Posts extends Component {
    componentDidMount(){
        this.props.getPosts();
    }

    componentDidUpdate(prevProps) {
        return prevProps !== this.props;
    }
    deletePost = id => {
        this.props.deletePost(id)
    }

    render(){
        const keys = Object.keys(this.props.posts);
        const p = this.props.posts;
        return (
            <Grid>
                <div className="PostsWrapper">
                    {console.log(this.props.posts)}
                    {keys.map(key => (
                        <Post
                            key={key}
                            title={p[key].title}
                            date={p[key].date_added}
                            image={p[key].image}
                            clicked={() => this.props.history.push(`/news/${p[key].id}`)}
                            delete={() => this.deletePost(p[key].id)}
                        />
                    ))}
                </div>
            </Grid>
        )
    }
}

const mapStateToProps = state => ({
    posts: state.posts
});

const mapDispatchToProps = dispatch => ({
    getPosts: () => dispatch(getPosts()),
    deletePost: (id) => dispatch(deletePost(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Posts);