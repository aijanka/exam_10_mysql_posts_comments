import React, {Component, Fragment} from 'react';
import {Route, Switch} from "react-router-dom";
import Header from "./components/Header";
import Posts from "./containers/Posts/Posts";
import PostFull from "./containers/PostFull/PostFull";
import AddPost from "./containers/AddPost/AddPost";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Header/>
                <main>
                    <Switch>
                        <Route path='/news' exact component={Posts}/>
                        <Route path='/news/:id' exact component={PostFull}/>
                        <Route path='/addPost' exact component={AddPost}/>
                    </Switch>

                </main>
            </Fragment>
        );
    }
}

export default App;
