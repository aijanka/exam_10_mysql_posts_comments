import * as actionTypes from './actionTypes';
import {ADD_SUCCESS} from "./actionTypes";
const initialState = {
    posts: [],
    comments: [],
    currentPost: [],
    currentComments: [],
    error: null
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.POSTS_SUCCESS: 
            return {...state, posts: action.posts};
        case actionTypes.POSTS_FAILURE:
            return {...state, error: action.error};
        case actionTypes.POST_SUCCESS:
            return {...state, currentPost: action.post};
        case actionTypes.COMMENTS_SUCCESS:
            return {...state, currentComments: action.comments};
        case actionTypes.ADD_COMMENT_SUCCESS:
            const comments = state.comments;
            comments.push(action.comment);
            return {...state, comments};
        case ADD_SUCCESS:
            const posts = [...state.posts];
            posts.push(action.post);
            console.log(posts, 'posts in reducer');
            return {...state, posts};
        default: 
            return state;
    }
};

export default reducer;