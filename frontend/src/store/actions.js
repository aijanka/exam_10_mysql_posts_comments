import * as actionTypes from './actionTypes';
import axios from 'axios';

export const getPostsSuccess = (posts) => ({type: actionTypes.POSTS_SUCCESS, posts});
export const getPostsFailure = error => ({type: actionTypes.POSTS_FAILURE, error});
export const getPosts = () => {
    return (dispatch) => {
        return axios.get('/news').then(response => {
            console.log(response.data);
            dispatch(getPostsSuccess(response.data));
        }, error => {
            dispatch(getPostsFailure(error));
        });
    };
};

export const getPostSuccess = post => ({type: actionTypes.POST_SUCCESS, post});
export const getPost = id => {
    return (dispatch) => {
        return axios.get(`/news/${id}`).then(response => {
            console.log(response.data);
            dispatch(getPostSuccess(response.data[0]));
            return response.data[0];
        })
    }
}

export const getCommentsSuccess = comments => ({type: actionTypes.COMMENTS_SUCCESS, comments});
export const getComments = id => {
    return (dispatch) => {
        return axios.get(`/comments?news_id=${id}`).then(response => {
            console.log(response.data);
            dispatch(getCommentsSuccess(response.data));
            return response.data[0];
        })
    }
};

export const addCommentSuccess = comment => ({type: actionTypes.ADD_COMMENT_SUCCESS, comment});
export const addComment = (comment) => {
    return (dispatch) => {
        return axios.post(`/comments`, comment).then(response => {
            console.log(response.data);
            dispatch(addCommentSuccess(response.data));
        })
    }
};

export const addSuccess = post => ({type: actionTypes.ADD_SUCCESS, post});
export const addPost = (post) => {
    console.log(post, 'in actions')
    return (dispatch) => {
        axios.post('/news', post).then(response => {
            console.log(response.data, 'in addPost response.data')
            dispatch(addSuccess(response.data));
        })
    }
};

export const deleteSuccess = post => ({type: actionTypes.DELETE_SUCCESS, post});
export const deletePost = (id) => {
    return (dispatch) => {
        axios.delete(`/news/${id}`).then(response => {
            dispatch(deleteSuccess(response.data));
        })
    }
};

