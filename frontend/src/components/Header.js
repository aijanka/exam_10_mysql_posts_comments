import React from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from 'react-router-bootstrap';

const Header = () => (
    <Navbar>
        <Navbar.Header>
            <Navbar.Brand>
                <LinkContainer to='/news'><NavItem>News</NavItem></LinkContainer>
            </Navbar.Brand>
            <Nav pullRight>
                <LinkContainer to='addPost'>
                    <NavItem>Add new post</NavItem>
                </LinkContainer>
            </Nav>
        </Navbar.Header>
    </Navbar>
);

export default Header;