import React from 'react';
import {Button, Panel} from "react-bootstrap";

const Post = props => (
    <Panel>
        <Panel.Body>{props.title}</Panel.Body>
        <Panel.Footer>{props.date}</Panel.Footer>
        <Button onClick={props.delete}>Delete</Button>
        <Button onClick={props.clicked}>View full</Button>
    </Panel>
);

export default Post;